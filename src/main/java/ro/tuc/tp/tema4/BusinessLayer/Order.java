package ro.tuc.tp.tema4.BusinessLayer;

import java.io.Serializable;
import java.util.Date;

/** Reprezinta clasa care structureaza atributele unei comenzi. */
public class Order implements Serializable {
    /** ID-ul comenzii */
    private Integer orderID;
    /** ID-ul clientului care a plasat comanda */
    private Integer clientID;
    /** Data in care a fost plasata comanda */
    private Date orderDate;
    /** Pretul total al comenzii */
    private Integer price;

    /** Creaza un obiect din clasa Order
     * @param orderID ID-ul comenzii
     * @param clientID ID-ul clientului care a plasat comanda
     * @param orderDate Data in care a fost plasata comanda
     * @param price Pretul total al comenzii */
    public Order(Integer orderID, Integer clientID, Date orderDate, Integer price) {
        this.clientID = clientID;
        this.orderID = orderID;
        this.orderDate = orderDate;
        this.price = price;
    }

    @Override
    public int hashCode() {
        int result = 18;
        result = 31 * result + orderID;
        result = 31 * result + clientID;
        result = 31 * result + orderDate.hashCode();
        result = 31 * result + price;
        return result;
    }

    public Integer getOrderID() {
        return orderID; }
    public Integer getClientID() {
        return clientID; }
    public Date getOrderDate() {
        return orderDate; }
    public Integer getPrice() {
        return price; }

    public String toString() {
        String s = orderID + " " + clientID + " " + orderDate + " " + price + "\n";
        return s;
    }
}
