package ro.tuc.tp.tema4.BusinessLayer;

import java.util.ArrayList;

/** Reprezinta o produs care are in compozitia sa unul sau mai multe produse de baza din clasa BaseProduct. */
public class CompositeProduct extends MenuItem {
    /** Lista produselor de baza pe care le contine produsul.*/
    private ArrayList<MenuItem> composition;

    /** Creaza un obiect din clasa CompositeProduct, atribuind campurilor clasei valorile trimise ca parametru.
     * @param title Titlul / denumirea produsului
     * @param products Obiect de tipul ArrayList continant obiecte care reprezinta produsele de baza / compuse care fac parte din compozitia produsului curent.*/
    public CompositeProduct(String title, ArrayList<MenuItem> products) {
        super(
                title,
                products.stream().map(MenuItem::getRating).reduce(0.0, Double::sum),
                products.stream().map(MenuItem::getCalories).reduce(0, Integer::sum),
                products.stream().map(MenuItem::getProteins).reduce(0, Integer::sum),
                products.stream().map(MenuItem::getFat).reduce(0, Integer::sum),
                products.stream().map(MenuItem::getSodium).reduce(0, Integer::sum),
                products.stream().map(MenuItem::getPrice).reduce(0, Integer::sum)
        );
        composition = new ArrayList<>();
        composition.addAll(products);
    }

    public String toString() {
        StringBuilder res = new StringBuilder(super.toString());
        res.append("Composition:\n");
        for(MenuItem m : composition)
            res.append(m);
        return res.toString();
    }
}
