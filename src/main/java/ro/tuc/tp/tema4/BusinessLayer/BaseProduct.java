package ro.tuc.tp.tema4.BusinessLayer;

/** Reprezinta un produs de baza care poate face parte din compozitia unei comenzi. */
public class BaseProduct extends MenuItem {
    /** Creaza un obiect din clasa BaseProduct, atribuind campurilor clasei valorile trimise ca parametru.
     * @param title Titlul / denumirea produsului
     * @param rating Rating-ul produsului
     * @param calories Cantitatea de calorii a produsului
     * @param proteins Cantitatea de proteine a produsului
     * @param fat Cantiatea de grasimi a produsului
     * @param sodium Cantitatea de sodiu a produsului
     * @param price Pretul produsului */
    public BaseProduct(String title, Double rating, Integer calories, Integer proteins, Integer fat, Integer sodium, Integer price) {
        super(title, rating, calories, proteins, fat, sodium, price);
    }
}
