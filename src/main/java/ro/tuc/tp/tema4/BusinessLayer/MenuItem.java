package ro.tuc.tp.tema4.BusinessLayer;

import java.io.Serializable;

/** Reprezinta clasa obstracta care structureaza atributele comune ale unui produs de baza si ale unui produs compus*/
public abstract class MenuItem implements Serializable {
    /** Titlul / denumirea produsului */
    private String title;
    /** Rating-ul produsului */
    private Double rating;
    /** Cantitatea de calorii a produsului */
    private Integer calories;
    /** Cantitatea de proteine a produsului */
    private Integer proteins;
    /** Cantitatea de grasimi a produsului */
    private Integer fat;
    /** Cantitatea de sodiu a produsului */
    private Integer sodium;
    /** Pretul produsului */
    private Integer price;

    /** Creaza un obiect dintr-o clasa care mosteneste clasa MenuItem
     * @param title Titlul / denumirea produsului
     * @param rating Rating-ul produsului
     * @param calories Cantitatea de calorii a produsului
     * @param proteins Cantitatea de proteine a produsului
     * @param fat Cantitatea de grasimi a produsului
     * @param sodium Cantitatea de sodiu a produsului
     * @param price Pretul produsului */
    public MenuItem(String title, Double rating, Integer calories, Integer proteins, Integer fat, Integer sodium, Integer price) {
        this.title = title;
        this.rating = rating;
        this.calories = calories;
        this.proteins = proteins;
        this.fat = fat;
        this.sodium = sodium;
        this.price = price;
    }

    public String getTitle() {
        return title; }
    public Double getRating() {
        return rating; }
    public Integer getCalories() {
        return calories; }
    public Integer getFat() {
        return fat; }
    public Integer getSodium() {
        return sodium; }
    public Integer getPrice() {
        return price; }
    public Integer getProteins() {
        return this.proteins; }

    public String toString() {
        return title + ", Rating: " + rating + ", Calories: " + calories + ", Proteins: " + proteins + ", Fat: " + fat + ", Sodium: " + sodium + ", Price: " + price + '\n';
    }

    @Override
    public int hashCode() {
        int result = 10;
        result = 31 * result + title.hashCode();
        result = 31 * result + rating.hashCode();
        result = 31 * result + calories.hashCode();
        result = 31 * result + proteins.hashCode();
        result = 31 * result + fat.hashCode();
        result = 31 * result + sodium.hashCode();
        result = 31 * result + price.hashCode();
        return result;
    }
}
