package ro.tuc.tp.tema4.BusinessLayer;

/** Tipurile de utilizator existente */
public enum Status {
    CLIENT, EMPLOYEE, ADMIN
}
