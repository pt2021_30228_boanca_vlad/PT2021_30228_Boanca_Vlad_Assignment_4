package ro.tuc.tp.tema4.BusinessLayer;

import java.io.Serializable;
/** Reprezinta informatiile despre un cont de utilizator. */
public class Account implements Serializable {
    /** ID-ul utilizatorului */
    private Integer id;
    /** Numele utilizatorului */
    private String user;
    /** Parola utilizatorului */
    private String pass;
    /** Tipul de utilizator */
    private Status status;
    /** Numarul de comenzi efectuate de utilizator */
    private Integer numberOfOrders;

    /** Creaza un obiect din clasa Account cu numele de utilizator si parola trnasmise ca parametrii. */
    public Account(String user, String pass) {
        this.user = user;
        this.pass = pass;
        this.id = this.hashCode();
        this.status = Status.CLIENT;
        numberOfOrders = 0;
    }

    public String getPass() {
        return pass; }
    public String getUser() {
        return user; }
    public Integer getId() {
        return id; }
    public Status getStatus() {
        return status; }
    public void setId(Integer id) {
        this.id = id; }
    public void setStatus(Status status) {
        this.status = status; }
    public void setNumberOfOrders(Integer numberOfOrders) {
        this.numberOfOrders = numberOfOrders; }
    public void incrementNumberOfOrders() {
        this.numberOfOrders++; }

    @Override
    public int hashCode() {
        int result = 18;
        result = 31 * result + user.hashCode();
        result = 31 * result + pass.hashCode();
        return result;
    }
}

