package ro.tuc.tp.tema4.BusinessLayer;

import java.io.IOException;
import java.util.*;

/** Interfata care defineste metodele care urmeaza sa fie implementate de clasa DeliveryService */
public interface IDeliveryServiceProcessing {

    /**Importa produsele disponibile dintr-un fisier cu extensia ".csv" in structurile de date corespunzatoare.
     * @throws IOException
     * @throws ClassNotFoundException */
    void importProducts() throws IOException, ClassNotFoundException;

    /** Adauga un produs in colectia celor deja existente.
     * @param product Produsul care se doreste a fi adaugat */
    void addProduct(MenuItem product);
    /** Editeaza atributele unui produs deja existent, primit ca parametru.
     * @param product Produsul pentru care se doreste editarea
     * @pre product != null */
    void editProduct(MenuItem product);
    /** Sterge un produs deja existent, primit ca parametru.
     * @param product Produsul pentru care se doreste editarea
     * @pre product != null */
    void deleteProduct(MenuItem product);

    /** Calculeaza pretul comenzii pana in momentul curent.
     * @param currentOrder Lista cu produsele care fac parte din comanda curenta
     * @return Intreg reprezentand pretul total al comenzii
     * @post @result >= 0 */
    int computeOrderPrice(ArrayList<MenuItem> currentOrder);

    /** Verifica daca numarul de comenzi este mereu valid, in concordanta cu comenzile existente.
     * @return */
    boolean isWellFormed();

    /** Filtreaza produsele existente in functie de continutul titlului dorit de utilizator.
     * @param title Titlul produselor
     * @return Map continand produsele care corespund criteriilor */
    Map<Integer, MenuItem> findByTitle(String title);

    /** Filtreaza produsele existente in functie de rating-ul dorit de utilizator.
     * @param rating Rating-ul produselor
     * @return Map continand produsele care corespund criteriilor */
    Map<Integer, MenuItem> findByRating(String rating);

    /** Filtreaza produsele existente in functie de cantitatea de calorii dorita de utilizator.
     * @param calories Cantitatea de calorii a produselor
     * @return Map continand produsele care corespund criteriilor */
    Map<Integer, MenuItem> findByCalories(String calories);

    /** Filtreaza produsele existente in functie de cantitatea de proteine dorita de utilizator.
     * @param proteins Cantitatea de proteine a produselor
     * @return Map continand produsele care corespund criteriilor */
    Map<Integer, MenuItem> findByProteins(String proteins);

    /** Filtreaza produsele existente in functie de cantitatea de grasimi dorita de utilizator.
     * @param fat Cantitatea de grasimi a produselor
     * @return Map continand produsele care corespund criteriilor */
    Map<Integer, MenuItem> findByFat(String fat);

    /** Filtreaza produsele existente in functie de cantitatea de sodiu dorita de utilizator.
     * @param sodium Cantitatea de sodiu a produselor
     * @return Map continand produsele care corespund criteriilor */
    Map<Integer, MenuItem> findBySodium(String sodium);

    /** Filtreaza produsele existente in functie de pretul dorit de utilizator
     * @param price Pretul produselor
     * @return Map continand produsele care corespund criteriilor */
    Map<Integer, MenuItem> findByPrice(String price);

    /** Genereaza un raport continand comenzile plasate intr-un interval de timp specificat de administrator.
     * @param minHour Ora minima pentru efectuarea comenzii
     * @param maxHour Ora maxima pentru efectuarea comenzii
     * @return Set continand comenzile care corespund criteriilor */
    Set<Order> generateReportA(int minHour, int maxHour);

    /** Genereaza un raport continand produsele comandate de un anumit numar de ori specifiat de administrator.
     * @param minAmount Numarul minim de comenzi ale unui produs
     * @return Set continand produsele care corespund criteriilor */
    Set<MenuItem> generateReportB(int minAmount);

    /** Genereaza un raport continand clientii care au efectuat un anumit numar de comenzi, cu pretul total mai mare decat o anumita valoare.
     * @param orderAmount Numarul de comenzi efectuate de catre un client
     * @param minPrice Pretul minim al unei comenzi
     * @return Set continand utilizatorii care corespunz criteriilor */
    Set<Account> generateReportC(int orderAmount, int minPrice);

    /** Genereaza un raport continand produsele comandate intr-o zi anume.
     * @param date Data reprezentand ziua in care a fost efectuata o comanda
     * @return HashMap continand produsele care corespund criteriilor */
    HashMap<Integer, Integer> generateReportD(Date date);
}
