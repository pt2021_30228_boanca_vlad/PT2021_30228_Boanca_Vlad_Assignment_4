package ro.tuc.tp.tema4.BusinessLayer;
import ro.tuc.tp.tema4.DataLayer.*;
import ro.tuc.tp.tema4.DataLayer.FileWriter;
import ro.tuc.tp.tema4.PresentationLayer.*;
import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/** @invariant isWellFormed() */

/** Reprezinta clasa care face conexiunea intre interfata grafica si logica programului. */
public class DeliveryService extends Observable implements IDeliveryServiceProcessing {
    /** Stocheaza informatiile despre comenzi, facand asocierea intre fiecare comanda si lista corespunzatoare de produse comandate. */
    private HashMap<Order, ArrayList<MenuItem>> orderInformation;
    /** Stocheaza informatii despre produsele disponibile pentru comanda. */
    private HashMap<Integer, MenuItem> products, productsForFiltering, productsAddedByAdmin;
    /** Stocheaza informatii despre cantitatea comandata pentru fiecare produs existent. */
    private HashMap<Integer, Integer> productsOrderCount;
    /** Stocheaza informatii despre conturile utilizatorilor existenti. */
    private HashMap<Integer, Account> userInfo;
    /** Stocheaza informatii despre produsele care urmeaza sa compuna un produs compus si despre produsele care urmeaza sa faca parte dintr-o comanda. */
    private ArrayList<MenuItem> productsForComposite, currentOrderProducts;
    /** Stocheaza informatii despre cea mai recenta comanda. */
    private Order currentOrder;
    /** Stocheaza informatii despre contul utilizatorului curent al programului. */
    private Account currentAccount;
    /** Stocheaza informatii despre numarul de comenzi efectuate. */
    private Integer numberOfOrders;
    /** Reprezinta interfata grafica utilizata pentru autentificarea unui utilizator. */
    private Login loginFrame;
    /** Reprezinta interfata grafica utilizata de catre un client pentru interactiunea cu programul.*/
    private Client clientFrame;
    /** Reprezinta interfata grafica utilizata de catre administrator pentru interactiunea cu programul. */
    private Administrator adminFrame;
    /** Reprezinta interfata grafica utilizata de catre angajat pentru ineractiunea cu programul. */
    private Employee employeeFrame;
    /** Reprezinta interfata grafica utilizata de catre administrator pentru generarea a diferitor rapoarte. */
    private Reports reportsFrame;
    /** Reprezinta interfata grafica utilizata de catre administrator pentru introducerea / editarea / stergerea unui produs. */
    private Product productFrame;
    /** Variabila de control care indica daca s-a efectual prima autentificare a sesiunii curente. */
    private boolean isFirst;
    /** Creaza un obiect din clasa DeliveryService. */
    public DeliveryService() {
        isFirst = true;
        userInfo = new HashMap<>();
        products = new HashMap<>();
        productsAddedByAdmin = new HashMap<>();
        productsForFiltering = new HashMap<>();
        productsOrderCount = new HashMap<>();
        orderInformation = new HashMap<>();
        currentOrderProducts = new ArrayList<>();
        productsForComposite = new ArrayList<>();
        initLoginFrame();
    }
    @Override
    public void importProducts() throws FileNotFoundException {
        File inputFile = new File("/Users/boancavlad/Desktop/TP/Laborator/Tema4/products.csv");
        InputStream inputStream = new FileInputStream(inputFile);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        products = (HashMap<Integer, MenuItem>)br.lines().skip(1).map(mapToItem).filter(distinctByKey(MenuItem::getTitle)).collect(Collectors.toMap(MenuItem::hashCode, p -> p));
        productsForFiltering.putAll(products);
    }

    private Function<String, MenuItem> mapToItem = (line) -> {
        String[] attributes = line.split(",");
        MenuItem m = createProduct(attributes);
        return m;
    };

    private <T>Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    /** Initializeaza interfata grafica utilizata pentru autentificarea utilizatorului in program. */
    private void initLoginFrame() {
        loginFrame = new Login();
        loginFrame.getLoginButton().addActionListener(e -> validateLogin());
        loginFrame.getRegisterButton().addActionListener(e -> validateRegistration());
        loginFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                if(isFirst) {
                    userInfo.putAll((HashMap<Integer, Account>)Serializator.deserialize("userinfo.ser"));
                    orderInformation.putAll((HashMap<Order, ArrayList<MenuItem>>)Serializator.deserialize("orderinfo.ser"));
                    productsAddedByAdmin.putAll((HashMap<Integer, MenuItem>)Serializator.deserialize("products_by_admin.ser"));
                    products.putAll(productsAddedByAdmin);
                    productsOrderCount.putAll((HashMap<Integer, Integer>)Serializator.deserialize("productsinfo.ser"));
                    numberOfOrders = orderInformation.size();
                    isFirst = false;
                    assert isWellFormed();
                }
            }
            @Override
            public void windowClosing(WindowEvent e) {
                Serializator.serialize("userinfo.ser", userInfo);
                Serializator.serialize("orderinfo.ser", orderInformation);
                Serializator.serialize("productsinfo.ser", productsOrderCount);
                Serializator.serialize("products_by_admin.ser", productsAddedByAdmin);
            }
        });
    }
    /** Initializeaza interfata grafica utilizata de catre administrator pentru interactiunea cu programul. */
    private void initAdminFrame() {
        adminFrame = new Administrator(new HashMap<>());
        adminFrame.getLogOutButton().addActionListener(e -> logOut(adminFrame));
        adminFrame.getAddProductButton().addActionListener(e -> addProduct(null));
        adminFrame.getDeleteProductButton().addActionListener(e -> deleteProduct(products.get(adminFrame.getSelectedRowHashValue())));
        adminFrame.getEditProductButton().addActionListener(e -> editProduct(products.get(adminFrame.getSelectedRowHashValue())));
        adminFrame.getGenerateReportsButton().addActionListener(e -> {
            reportsFrame = new Reports();
            addReportsListeners();
        });
        adminFrame.getAddToComposedProduct().addActionListener(e -> {
            productsForComposite.add(productsForFiltering.get(adminFrame.getSelectedRowHashValue()));
            adminFrame.setCompositeProductItems(productsForComposite.size() + " products in composition");
        });
        adminFrame.getCreateProductButton().addActionListener(e -> {
            String title = JOptionPane.showInputDialog(adminFrame, "Enter the wanted name for the new product:");
            addProduct(new CompositeProduct(title, productsForComposite));
        });
        adminFrame.getImportProductsButton().addActionListener(e -> {
            try {
                importProducts();
                adminFrame.initializePanel(products);
                adminFrame.enableFiltering();
            } catch (Exception ex) { ex.printStackTrace(); }
        });
    }
    /** Initializeaza interfata grafica utilizata de catre client pentru interactiunea cu programul. */
    private void initClientFrame() {
        try { importProducts(); }
        catch (Exception ex) { ex.printStackTrace(); }
        clientFrame = new Client(products);
        clientFrame.initializePanel(products);
        addClientFrameListeners();
        addClientTableListener();
    }
    /** Initializeaza interfata grafica utilizata de catre angajat pentru interactiunea cu programul. */
    private void initEmployeeFrame() {
        employeeFrame = new Employee(orderInformation);
        addEmployeeFrameListeners();
    }
    /** Adauga ascultatori pentru butoanele din interfata grafica utilizata de catre angajat. */
    private void addEmployeeFrameListeners() {
        employeeFrame.getSeeSelectedOrderButton().addActionListener(e -> seeOrderItems(employeeFrame.getSelectedOrder()));
        employeeFrame.getLogOutButton().addActionListener(e -> logOut(employeeFrame));
        this.addObserver(employeeFrame);
    }
    /** Adauga ascultatori pentru tabelul din interfata grafica utilizata de catre client. */
    private void addClientTableListener() {
        clientFrame.getProductsTable().getSelectionModel().addListSelectionListener(e -> {
            if(productsForFiltering.get(clientFrame.getSelectedRowHashValue()) instanceof CompositeProduct)
                clientFrame.enableComposition();
            else clientFrame.disableComposition();
        });
    }
    /** Valideaza autentificarea unui utilizator. */
    private void validateLogin() {
        for(Account a : userInfo.values()) {
            if(a.getUser().equals(loginFrame.getUserFieldValue()) && a.getPass().equals(loginFrame.getPassFieldValue())) {
                currentAccount = a;
                if(a.getStatus() == Status.ADMIN) {
                    initAdminFrame();
                } else if(a.getStatus() == Status.CLIENT) {
                    initClientFrame();
                    initEmployeeFrame();
                } else
                    initEmployeeFrame();
                loginFrame.dispose();
                return;
        } }
        JOptionPane.showMessageDialog(loginFrame, "Invalid username or password.");
    }
    /** Valideaza inregistrarea unui nou utilizator. */
    private void validateRegistration() {
        for(Account a : userInfo.values())
            if(a.getUser().equals(loginFrame.getUserFieldValue())) {
                JOptionPane.showMessageDialog(loginFrame, "This username is already taken.");
                return;
            }
        currentAccount = new Account(loginFrame.getUserFieldValue(), loginFrame.getPassFieldValue());
        userInfo.put(currentAccount.getId(), currentAccount);
        JOptionPane.showMessageDialog(loginFrame, "Account created.");
    }
    /** Adauga ascultatori pentru butoanele din interfata grafica utilizata de catre client. */
    private void addClientFrameListeners() {
        clientFrame.getFilterProductsButton().addActionListener(e -> {
            productsForFiltering.putAll(products);
            FilterBlock[] fb = clientFrame.getFilterBlocks();
            for(int i = 0; i < clientFrame.getCurrentNumberOfFilters(); i++) {
                if(fb[i].getFilterProductsField().getText() != null) {
                    switch (fb[i].getFilterProductsCombo().getSelectedIndex()) {
                        case 0: productsForFiltering = (HashMap<Integer, MenuItem>) findByTitle(fb[i].getFilterProductsField().getText()); break;
                        case 1: productsForFiltering = (HashMap<Integer, MenuItem>) findByRating(fb[i].getFilterProductsField().getText()); break;
                        case 2: productsForFiltering = (HashMap<Integer, MenuItem>) findByCalories(fb[i].getFilterProductsField().getText()); break;
                        case 3: productsForFiltering = (HashMap<Integer, MenuItem>) findByProteins(fb[i].getFilterProductsField().getText()); break;
                        case 4: productsForFiltering = (HashMap<Integer, MenuItem>) findByFat(fb[i].getFilterProductsField().getText()); break;
                        case 5: productsForFiltering = (HashMap<Integer, MenuItem>) findBySodium(fb[i].getFilterProductsField().getText()); break;
                        case 6: productsForFiltering = (HashMap<Integer, MenuItem>) findByPrice(fb[i].getFilterProductsField().getText()); break;
            } } }
            clientFrame.initializePanel(productsForFiltering);
            addClientTableListener();
        });
        clientFrame.getResetFiltersButton().addActionListener(e -> {
            clientFrame.setCurrentNumberOfFilters(1);
            clientFrame.initializePanel(products);
            addClientTableListener();
            productsForFiltering.putAll(products);
        });
        clientFrame.getAddFilterButton().addActionListener(e -> {
            clientFrame.setCurrentNumberOfFilters(clientFrame.getCurrentNumberOfFilters() + 1);
            clientFrame.initializePanel(products);
            addClientTableListener();
            productsForFiltering.putAll(products);
        });
        clientFrame.getClearOrderButton().addActionListener(e -> clearOrder());
        clientFrame.getPlaceOrderButton().addActionListener(e -> placeOrder());
        clientFrame.getLogOutButton().addActionListener(e -> logOut(clientFrame));
        clientFrame.getAddToOrderButton().addActionListener(e -> addToOrder(productsForFiltering.get(clientFrame.getSelectedRowHashValue())));
        clientFrame.getSeeCompositionButton().addActionListener(e -> JOptionPane.showMessageDialog(clientFrame, productsForFiltering.get(clientFrame.getSelectedRowHashValue())));
    }
    /** Adauga ascultatori pentru butoanele din interfata utilizata de administrator pentru generarea rapoartelor. */
    private void addReportsListeners() {
        reportsFrame.getGenerateReportAButton().addActionListener(e -> {
            try { FileWriter.writeReportA(generateReportA(reportsFrame.getMinHour(), reportsFrame.getMaxHour())); }
            catch (IOException ioException) { ioException.printStackTrace(); }
        });
        reportsFrame.getGenerateReportBButton().addActionListener(e -> {
            try { FileWriter.writeReportB(generateReportB(reportsFrame.getMinProductAmountB()), productsOrderCount); }
            catch (IOException ioException) { ioException.printStackTrace(); }
        });
        reportsFrame.getGenerateReportCButton().addActionListener(e -> {
            try { FileWriter.writeReportC(generateReportC(reportsFrame.getOrderAmount(), reportsFrame.getMinPrice())); }
            catch (IOException ioException) { ioException.printStackTrace(); }
        });
        reportsFrame.getGenerateReportDButton().addActionListener(e -> {
            try { FileWriter.writeReportD(generateReportD(reportsFrame.getDate()), products); }
            catch (ParseException | IOException parseException) { parseException.printStackTrace(); }
        });
    }
    /** Efectueaza operatiile necesare pentru plasarea unei comenzi. */
    private void placeOrder() {
        try {
            currentAccount.incrementNumberOfOrders();
            currentOrder = new Order(++numberOfOrders, currentAccount.getId(), new Date(System.currentTimeMillis()), computeOrderPrice(currentOrderProducts));
            orderInformation.put(currentOrder, currentOrderProducts);
            FileWriter.createBill(orderInformation, currentOrder);
            setChanged();
            notifyObservers(currentOrder);
            clearOrder();
            JOptionPane.showMessageDialog(clientFrame, "Order placed!");
            assert isWellFormed();
        } catch (IOException ioException) { ioException.printStackTrace(); }
    }
    /** Efectueaza operatiile necesare pentru adaugarea unui nou produs la comanda curenta. */
    private void addToOrder(MenuItem product) {
        currentOrderProducts.add(product);
        productsOrderCount.replace(product.hashCode(), productsOrderCount.get(product.hashCode()) + 1);
        clientFrame.getCurrentOrderLabel().setEnabled(true);
        clientFrame.getCurrentOrderLabel().setText("Products to order: " + currentOrderProducts.size());
        clientFrame.getOrderPriceLabel().setText("Total price: " + computeOrderPrice(currentOrderProducts));
    }
    /** Reseteaza comanda curenta, eliminand produsele adaugate pana in acel moment. */
    private void clearOrder() {
        currentOrderProducts = new ArrayList<>();
        clientFrame.getCurrentOrderLabel().setEnabled(false);
        clientFrame.getCurrentOrderLabel().setText("Products to order: 0");
        clientFrame.getOrderPriceLabel().setText("Total price: 0");
    }
    /** Permite angajatului sa vizualizeze produsele comandate pentru o comanda selectata. */
    private void seeOrderItems(Order order) {
        StringBuilder s = new StringBuilder();
        for(MenuItem m : orderInformation.get(order))
            s.append(m.toString()).append("\n");
        JOptionPane.showMessageDialog(employeeFrame, s.toString());
    }
    /** Efectueaza operatiile necesare dezautentificarii unui client. */
    private void logOut(JFrame frame) {
        currentAccount = null;
        frame.dispose();
        if(frame instanceof Client) {
            this.deleteObserver(employeeFrame);
            employeeFrame.dispose();
            employeeFrame = null;
        }
        initLoginFrame();
    }
    /** Actualizeaza structurile de date care stocheaza informatiile despre produsele existente si cantitatea lor comandata. */
    private void updateHashTables(MenuItem p) {
        productsAddedByAdmin.put(p.hashCode(), p);
        productsForFiltering.put(p.hashCode(), p);
        productsOrderCount.put(p.hashCode(), 0);
        products.put(p.hashCode(), p);
        adminFrame.initializePanel(products);
        JOptionPane.showMessageDialog(adminFrame, "Product added!");
    }
    /** Creaza un produs de baza reprezentat de un obiect din clasa BaseProduct. */
    private BaseProduct createProduct(String[] attributes) {
        return new BaseProduct(attributes[0], Double.parseDouble(attributes[1]), Integer.parseInt(attributes[2]), Integer.parseInt(attributes[3]), Integer.parseInt(attributes[4]), Integer.parseInt(attributes[5]), Integer.parseInt(attributes[6]));
    }
    @Override
    public void addProduct(MenuItem product) {
        if(product == null) {
            productFrame = new Product("Add new product");
            productFrame.getSubmitButton().addActionListener(e -> {
                BaseProduct p = new BaseProduct(productFrame.getTitleValue(), productFrame.getRatingValue(), productFrame.getCaloriesValue(), productFrame.getProteinsValue(), productFrame.getFatValue(), productFrame.getSodiumValue(), productFrame.getPriceValue());
                productFrame.dispose();
                updateHashTables(p);
            });
        } else {
            updateHashTables(product);
            adminFrame.setCompositeProductItems("0 products in composition");
        }
    }
    @Override
    public void editProduct(MenuItem product) {
        assert product != null;
        productFrame = new Product("Edit product", product);
        productFrame.getSubmitButton().addActionListener(e -> {
            BaseProduct p = new BaseProduct(productFrame.getTitleValue(), productFrame.getRatingValue(), productFrame.getCaloriesValue(), productFrame.getProteinsValue(), productFrame.getFatValue(), productFrame.getSodiumValue(), productFrame.getPriceValue());
            if(productsAddedByAdmin.containsKey(product.hashCode()))
                productsAddedByAdmin.put(p.hashCode(), p);
            productsForFiltering.put(p.hashCode(), p);
            productsOrderCount.put(p.hashCode(), 0);
            products.put(p.hashCode(), p);
            deleteProduct(product);
        });
    }
    @Override
    public void deleteProduct(MenuItem product) {
        assert product != null;
        products.remove(product.hashCode());
        productsAddedByAdmin.remove(product.hashCode());
        productsForFiltering.remove(product.hashCode());
        adminFrame.initializePanel(products);
    }

    @Override
    public int computeOrderPrice(ArrayList<MenuItem> currentOrder) {
        int sum = currentOrder.stream().map(MenuItem::getPrice).reduce(0, Integer::sum);
        assert sum >= 0;
        return sum;
    }

    @Override
    public boolean isWellFormed() {
        return numberOfOrders.equals(orderInformation.size());
    }

    @Override
    public Map<Integer, MenuItem> findByTitle(String title) {
        return productsForFiltering.values().stream().filter(p -> p.getTitle().contains(title)).collect(Collectors.toMap(MenuItem::hashCode, p -> p));
    }
    @Override
    public Map<Integer, MenuItem> findByRating(String rating) {
        return productsForFiltering.values().stream().filter(p -> p.getRating().equals(Double.parseDouble(rating))).collect(Collectors.toMap(MenuItem::hashCode, p -> p));
    }
    @Override
    public Map<Integer, MenuItem> findByCalories(String calories) {
        return productsForFiltering.values().stream().filter(p -> p.getCalories().equals(Integer.parseInt(calories))).collect(Collectors.toMap(MenuItem::hashCode, p -> p));
    }
    @Override
    public Map<Integer, MenuItem> findByProteins(String proteins) {
        return productsForFiltering.values().stream().filter(p -> p.getProteins().equals(Integer.parseInt(proteins))).collect(Collectors.toMap(MenuItem::hashCode, p -> p));
    }
    @Override
    public Map<Integer, MenuItem> findByFat(String fat) {
        return productsForFiltering.values().stream().filter(p -> p.getFat().equals(Integer.parseInt(fat))).collect(Collectors.toMap(MenuItem::hashCode, p -> p));
    }
    @Override
    public Map<Integer, MenuItem> findBySodium(String sodium) {
        return productsForFiltering.values().stream().filter(p -> p.getSodium().equals(Integer.parseInt(sodium))).collect(Collectors.toMap(MenuItem::hashCode, p -> p));
    }
    @Override
    public Map<Integer, MenuItem> findByPrice(String price) {
        return productsForFiltering.values().stream().filter(p -> p.getPrice().equals(Integer.parseInt(price))).collect(Collectors.toMap(MenuItem::hashCode, p -> p));
    }
    @Override
    public Set<Order> generateReportA(int minHour, int maxHour) {
        return ReportGenerator.generateReportA(minHour, maxHour, orderInformation);
    }
    @Override
    public Set<MenuItem> generateReportB(int minAmount) {
        return ReportGenerator.generateReportB(minAmount, productsForFiltering, productsOrderCount);
    }
    @Override
    public Set<Account> generateReportC(int orderAmount, int minPrice) {
        return ReportGenerator.generateReportC(orderAmount, minPrice, orderInformation, userInfo);
    }
    @Override
    public HashMap<Integer, Integer> generateReportD(Date date) {
        return ReportGenerator.generateReportD(date, orderInformation);
    }
}