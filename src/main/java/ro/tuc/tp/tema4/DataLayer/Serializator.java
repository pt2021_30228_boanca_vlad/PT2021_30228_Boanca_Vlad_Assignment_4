package ro.tuc.tp.tema4.DataLayer;

import java.io.*;

/** Clasa care efectueaza serializarea si deserializarea informatiilor necesare programului pentru rulare. */
public class Serializator {
    /** Serializeaza obiectul trimis ca parametru intr-un fisier cu numele fileName
     * @param fileName Numele fisierului
     * @param o Obiectul pentru care se doreste serializarea */
    public static void serialize(String fileName, Object o) {
        try {
            FileOutputStream fout = new FileOutputStream(fileName);
            ObjectOutputStream oout = new ObjectOutputStream(fout);
            oout.writeObject(o);
            oout.flush();
            oout.close();
        } catch (IOException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
    }

    /** Deserializeaza obiectul aflat in fieisrul cu numele fileName
     * @param fileName Numele fisierului
     * @return Object */
    public static Object deserialize(String fileName) {
        Object o = null;
        try {
            FileInputStream fin = new FileInputStream(fileName);
            ObjectInputStream oin = new ObjectInputStream(fin);
            o = oin.readObject();
            oin.close();
        } catch (IOException | ClassNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
        return o;
    }
}
