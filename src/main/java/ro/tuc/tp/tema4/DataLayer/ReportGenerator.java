package ro.tuc.tp.tema4.DataLayer;

import ro.tuc.tp.tema4.BusinessLayer.Account;
import ro.tuc.tp.tema4.BusinessLayer.MenuItem;
import ro.tuc.tp.tema4.BusinessLayer.Order;

import java.util.*;
import java.util.stream.Collectors;

/** Clasa care filtreaza datele programului conform criteriilor introduse de utilixato */
public class ReportGenerator {

    /** Genereaza un set de comenzi corespunzand criteriilor introduse de utilizator
     * @param minHour Ora minima de efectuare a comenzii
     * @param maxHour Ora maxima de efectuare a comenzii
     * @param orderInformation Informatii despre comenzile existente
     * @return Set de comenzi*/
    public static Set<Order> generateReportA(int minHour, int maxHour,  HashMap<Order, ArrayList<MenuItem>> orderInformation) {
        return orderInformation.keySet().stream().filter(p -> p.getOrderDate().getHours() >= minHour && p.getOrderDate().getHours() <= maxHour).collect(Collectors.toSet());
    }

    /** Genereaza un set de produse corespunzand criteriilor introduse de utilizator
     * @param minAmount Numarul minim de comenzi ale unui produs
     * @param productsForFiltering Produsele existente
     * @param productsOrderCount Cantiatile comandate din fiecare produs
     * @return Set de produse*/
    public static Set<MenuItem> generateReportB(int minAmount, HashMap<Integer, MenuItem> productsForFiltering, HashMap<Integer, Integer> productsOrderCount) {
        return productsForFiltering.values().stream().filter(p -> productsOrderCount.get(p.hashCode()) > minAmount).collect(Collectors.toSet());
    }

    /** Genereaza un set de conturi de utilizator corespunzatoare criteriilor introduse de utilizator
     * @param orderAmount Numarul de comenzi efectuate de catre un client
     * @param minPrice Pretul minim al unei comenzi
     * @param orderInformation Informatii despre comenzile existente
     * @param userInfo Informatii despre utilizatorii existenti
     * @return Set de conturi */
    public static Set<Account> generateReportC(int orderAmount, int minPrice, HashMap<Order, ArrayList<MenuItem>> orderInformation, HashMap<Integer, Account> userInfo) {
        Set<Order> set = orderInformation.keySet().stream().filter(p -> p.getPrice() >= minPrice).collect(Collectors.toSet());
        HashMap<Integer, Integer> map = new HashMap<>();
        Set<Account> acc = new HashSet<>();
        for (Order o : set) {
            if (map.containsKey(o.getClientID())) {
                map.replace(o.getClientID(), map.get(o.getClientID()) + 1);
                if (map.get(o.getClientID()) >= orderAmount)
                    acc.add(userInfo.get(o.getClientID()));
            } else map.put(o.getClientID(), 1);
        }
        return acc;
    }

    /** Genereaza un HashMap de intregi reprezentand cantitatile produselor corespunzatoare criteriilor introduse de utilizator
     * @param date Data in care au fost efectuate comenziile
     * @param orderInformation HashMap cu informatiile despre comenzile efectuate
     * @return HashMap de intregi */
    public static HashMap<Integer, Integer> generateReportD(Date date, HashMap<Order, ArrayList<MenuItem>> orderInformation) {
        HashMap<Order, ArrayList<MenuItem>> orders = (HashMap<Order, ArrayList<MenuItem>>) orderInformation.keySet().stream().filter(p -> p.getOrderDate().getDay() == date.getDay()).collect(Collectors.toMap(p -> p, o -> orderInformation.get(o)));
        HashMap<Integer, Integer> map = new HashMap<>();
        for (Order o : orders.keySet()) {
            for (MenuItem m : orders.get(o)) {
                if (map.containsKey(m.hashCode()))
                    map.replace(m.hashCode(), map.get(m.hashCode()) + 1);
                else
                    map.put(m.hashCode(), 1);
            }
        }
        return map;
    }
}
