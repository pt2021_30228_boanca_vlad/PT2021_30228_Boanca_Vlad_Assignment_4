package ro.tuc.tp.tema4.DataLayer;
import ro.tuc.tp.tema4.BusinessLayer.Account;
import ro.tuc.tp.tema4.BusinessLayer.MenuItem;
import ro.tuc.tp.tema4.BusinessLayer.Order;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
/** Contine metode utilizate pentru crearea facturilor pentru comenzi si a rapoartelor. */
public class FileWriter {
    private static StringBuilder output = new StringBuilder();
    private static BufferedWriter writer;
    private static String fileName;

    /** Creaza o factura intr-un fisier text, care contine data comenzii, id-ul ei, id-ul clientului si produsele comandate impreuna cu pretul fiecareia si pretul total al comenzii
     * @param orderInformation Contine informatii despre comenzile efectuate
     * @param currentOrder Comanda pentru care se creaza factura
     * @throws IOException */
    public static void createBill(HashMap<Order, ArrayList<MenuItem>> orderInformation, Order currentOrder) throws IOException {
        fileName = "bill_client#" + currentOrder.getClientID() + "_order#" + currentOrder.getOrderID() + ".txt";
        String delimiter = "-----------------------------------------------------------------------------------------------------------------\n\n";
        String prodDelimiter = "...........................................................";
        new File(fileName).delete();
        writer = new BufferedWriter(new java.io.FileWriter(fileName, true));
        output = new StringBuilder();
        output.append(delimiter);
        output.append("    DATE: ").append(currentOrder.getOrderDate()).append("\n");
        output.append("    ORDER ID: ").append(currentOrder.getOrderID()).append("\n");
        output.append("    CLIENT ID: ").append(currentOrder.getClientID()).append("\n\n");
        output.append("---------------------------------------------- ORDERED PRODUCTS -------------------------------------------------\n\n");
        for(MenuItem m : orderInformation.get(currentOrder))
            output.append("    ").append(m.getTitle()).append(prodDelimiter).append(m.getPrice()).append("\n\n");
        output.append("    TOTAL: ").append(prodDelimiter).append(currentOrder.getPrice()).append("\n\n");
        output.append(delimiter);
        writer.append(output);
        writer.flush();
        writer.close();
    }

    /** Creaza un fisier text continand raportul A
     * @param orders Set cu comenzile corespunzatoare
     * @throws IOException */
    public static void writeReportA(Set<Order> orders) throws IOException {
        fileName = "reportTypeA.txt";
        output = new StringBuilder();
        new File(fileName).delete();
        writer = new BufferedWriter(new java.io.FileWriter(fileName, true));
        for(Order o : orders)
            output.append(o);
        writer.append(output);
        writer.flush();
        writer.close();
    }

    /** Creaza un fisier text continand raportul B
     * @param products Set cu produsele corespunzatoare
     * @param count HashMap care stocheaza contorul de comanda al produselor
     * @throws IOException */
    public static void writeReportB(Set<MenuItem> products, HashMap<Integer, Integer> count) throws IOException {
        fileName = "reportTypeB.txt";
        output = new StringBuilder();
        new File(fileName).delete();
        writer = new BufferedWriter(new java.io.FileWriter(fileName, true));
        for(MenuItem m : products) {
            output.append(m.getTitle()).append(", Quantity: ").append(count.get(m.hashCode())).append("\n");
        }
        writer.append(output);
        writer.flush();
        writer.close();
    }

    /** Creaza un fisier text continand raportul C
     * @param accounts Set cu conturile corespunzatoare ale utilizatorilor
     * @throws IOException */
    public static void writeReportC(Set<Account> accounts) throws IOException {
        fileName = "reportTypeC.txt";
        output = new StringBuilder();
        new File(fileName).delete();
        writer = new BufferedWriter(new java.io.FileWriter(fileName, true));
        for(Account acc : accounts) {
            output.append(acc.getUser()).append("\n");
        }
        writer.append(output);
        writer.flush();
        writer.close();
    }

    /** Creaza un fisier text continand raportul D
     * @param prods HashMap continand produsele corespunzatoare
     * @param products HashMap continand toate produsele
     * @throws IOException */
    public static void writeReportD(HashMap<Integer, Integer> prods, HashMap<Integer, MenuItem> products) throws IOException {
        fileName = "reportTypeD.txt";
        output = new StringBuilder();
        new File(fileName).delete();
        writer = new BufferedWriter(new java.io.FileWriter(fileName, true));
        for(Integer i : prods.keySet()) {
            output.append(products.get(i).getTitle()).append(", Quantity: ").append(prods.get(i)).append("\n");
        }
        writer.append(output);
        writer.flush();
        writer.close();
    }

}
