package ro.tuc.tp.tema4.PresentationLayer;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import javax.swing.*;

/** Reprezinta interfata grafica utilizata de catre utilizator pentru autentificarea in program. */
public class Login extends JFrame {
    private JTextField userField;
    private JPasswordField passField;
    private JButton login;
    private JButton register;

    /** Creaza un obiect din clasa Login. */
    public Login() {
        initFrame();
    }

    /** Initializeaza fereastra de autentificare */
    private void initFrame() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Food Delivery Management System");
        this.setSize(350, 230);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        initPanel();
        this.setVisible(true);
    }
    /** Initializeaza continutul ferestrei de autentificare. */
    private void initPanel() {
        userField = new JTextField();
        passField = new JPasswordField();
        login = new JButton("Login");
        register = new JButton("No account? Register");
        FormLayout layout = new FormLayout(
                "20px, right:pref, 8px, 100px, 4px, 130px, 20px",
                "20px, pref, 20px, pref, 20px, pref, 10px, pref");
        PanelBuilder builder = new PanelBuilder(layout);
        CellConstraints cc = new CellConstraints();
        builder.addLabel("Username", cc.xy (2,  2));
        builder.add(userField, cc.xyw(4, 2, 3));
        builder.addLabel("Password", cc.xy (2,  4));
        builder.add(passField, cc.xyw(4, 4, 3));
        builder.add(login, cc.xyw(2, 6, 5));
        builder.add(register, cc.xyw(2, 8, 5));
        this.setContentPane(builder.getPanel());
    }

    public JButton getLoginButton() {
        return login;
    }

    public JButton getRegisterButton() {
        return register;
    }

    public String getUserFieldValue() {
        return userField.getText();
    }

    public String getPassFieldValue() {
        return String.valueOf(passField.getPassword());
    }
}
