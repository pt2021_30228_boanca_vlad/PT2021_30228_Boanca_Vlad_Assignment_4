package ro.tuc.tp.tema4.PresentationLayer;

import javax.swing.*;
/** Reprezinta un block pentru filtrarea produselor, compus dintr-o casuta text si un selector. */
public class FilterBlock {
    private JLabel filterFieldLabel;
    private JLabel filterComboLabel;
    private JTextField filterProductsField;
    private JComboBox<String> filterProductsCombo;
    /** Creaza un obiect din clasa FilterBlock. */
    public FilterBlock() {
        filterFieldLabel = new JLabel("Search:");
        filterComboLabel = new JLabel("Search by:");
        filterProductsField = new JTextField();
        filterProductsCombo = new JComboBox<>();
        filterProductsCombo.addItem("Title");
        filterProductsCombo.addItem("Rating");
        filterProductsCombo.addItem("Calories");
        filterProductsCombo.addItem("Proteins");
        filterProductsCombo.addItem("Fat");
        filterProductsCombo.addItem("Sodium");
        filterProductsCombo.addItem("Price");
    }

    public JTextField getFilterProductsField() {
        return filterProductsField;
    }

    public JComboBox<String> getFilterProductsCombo() {
        return filterProductsCombo;
    }
}
