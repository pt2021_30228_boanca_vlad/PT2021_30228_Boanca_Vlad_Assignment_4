package ro.tuc.tp.tema4.PresentationLayer;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import ro.tuc.tp.tema4.BusinessLayer.MenuItem;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import java.util.HashMap;
/** Reprezinta interfata grafica utilizata de catre client pentru efectuarea unei comenzi. */
public class Client extends JFrame {
    private int currentFilters;
    private int currentLayoutRow;
    private JLabel currentOrder;
    private JLabel orderPrice;
    private JButton filterProductsButton;
    private JButton resetFiltersButton;
    private JButton addFilterButton;
    private JButton clearOrder;
    private JButton addToOrder;
    private JButton placeOrder;
    private JButton logOut;
    private JButton seeComposition;
    private JComboBox<String> filterProductsCombo;
    private JScrollPane scrollPane;
    private JTable productsTable;
    private FilterBlock[] filterBlocks;

    /** Creaza un obiect din clasa Client */
    public Client(HashMap<Integer, MenuItem> products) {
        currentFilters = 1;
        filterProductsButton = new JButton("Filter products");
        resetFiltersButton = new JButton("Clear filters");
        addFilterButton = new JButton("Add new filter");
        addToOrder = new JButton("Add to bag");
        currentOrder = new JLabel("Products to order: 0");
        orderPrice = new JLabel("Total price: 0");
        currentOrder.setEnabled(false);
        clearOrder = new JButton("Clear order");
        placeOrder = new JButton("Place order");
        logOut = new JButton("Logout");
        seeComposition = new JButton("See composition");
        filterProductsCombo = new JComboBox<>();
        filterProductsCombo.addItem("Title");
        filterProductsCombo.addItem("Rating");
        filterProductsCombo.addItem("Calories");
        filterProductsCombo.addItem("Fat");
        filterProductsCombo.addItem("Sodium");
        filterProductsCombo.addItem("Price");
        filterBlocks = new FilterBlock[6];
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setTitle("Client Window");
        this.setSize(1050, 940);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        initializePanel(products);
        this.setVisible(true);
    }

    /** Initializeaza continutul ferestrei.
     * @param products Produsele existente */
    public void initializePanel(HashMap<Integer, MenuItem> products) {
        if(currentFilters == 6)
            addFilterButton.setEnabled(false);
        else
            addFilterButton.setEnabled(true);
        currentLayoutRow = 2;
        createTable(products);
        FormLayout layout = new FormLayout(
        "20px, 800px, 20px, 65px, 5px, 120px, 20px",
        "20px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 20px");
        PanelBuilder builder = new PanelBuilder(layout);
        CellConstraints cc = new CellConstraints();
        builder.add(scrollPane, cc.xywh(2, 2, 1, 43));
        for(int i = 0; i < currentFilters; i++) {
            filterBlocks[i] = new FilterBlock();
            builder.add(new JLabel("Filter:"), cc.xyw(4, currentLayoutRow, 1));
            builder.add(filterBlocks[i].getFilterProductsField(), cc.xyw(6, currentLayoutRow, 1));
            currentLayoutRow += 2;
            builder.add(new JLabel("Filter by:"), cc.xyw(4, currentLayoutRow, 1));
            builder.add(filterBlocks[i].getFilterProductsCombo(), cc.xyw(6, currentLayoutRow, 1));
            currentLayoutRow += 2;
        }
        builder.add(filterProductsButton, cc.xyw(4, currentLayoutRow, 3));
        builder.add(addFilterButton, cc.xyw(4, currentLayoutRow + 2, 3));
        builder.add(resetFiltersButton, cc.xyw(4, currentLayoutRow + 4, 3));
        builder.add(addToOrder, cc.xyw(4, currentLayoutRow + 6,3));
        builder.add(seeComposition, cc.xyw(4, currentLayoutRow + 8,3));
        builder.add(currentOrder, cc.xyw(4, 36, 3));
        builder.add(orderPrice, cc.xyw(4, 38, 3));
        builder.add(placeOrder, cc.xyw(4, 40, 3));
        builder.add(clearOrder, cc.xyw(4, 42, 3));
        builder.add(logOut, cc.xyw(4, 44, 3));
        this.setContentPane(builder.getPanel());
        this.repaint();
        this.revalidate();
    }

    /**
     * Creaza tabelul care contine produsele disponibile pentru comanda.
     * @param products Produsele existente. */
    public void createTable(HashMap<Integer, MenuItem> products) {
        String[] col = {"Title", "Rating", "Calories", "Proteins", "Fat", "Sodium", "Price", "Hashcode"};
        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        productsTable = new JTable(tableModel);
        if(products != null) {
            for(MenuItem p : products.values()) {
                Object[] arr = {p.getTitle(), p.getRating(), p.getCalories(), p.getProteins(), p.getFat(), p.getSodium(), p.getPrice(), p.hashCode()};
                tableModel.addRow(arr);
            }
            TableColumnModel tcm = productsTable.getColumnModel();
            tcm.removeColumn(tcm.getColumn(7));
        }
        scrollPane = new JScrollPane(productsTable);
    }

    public JTable getProductsTable() {
        return productsTable; }
    public JButton getFilterProductsButton() {
        return filterProductsButton; }
    public JButton getResetFiltersButton() {
        return resetFiltersButton; }
    public JButton getAddFilterButton() {
        return addFilterButton; }
    public JButton getAddToOrderButton() {
        return addToOrder; }
    public JButton getClearOrderButton() {
        return clearOrder; }
    public JButton getPlaceOrderButton() {
        return placeOrder; }
    public JButton getSeeCompositionButton() {
        return seeComposition; }
    public JButton getLogOutButton() {
        return logOut; }
    public JLabel getCurrentOrderLabel() {
        return currentOrder; }
    public JLabel getOrderPriceLabel() {
        return orderPrice; }
    public int getSelectedRowHashValue() {
        return (int) productsTable.getModel().getValueAt(productsTable.getSelectedRow(), 7); }
    public FilterBlock[] getFilterBlocks() {
        return filterBlocks; }
    public int getCurrentNumberOfFilters() {
        return currentFilters; }
    public void setCurrentNumberOfFilters(int currentFilters) {
        this.currentFilters = currentFilters; }
    public void enableComposition() {
        seeComposition.setEnabled(true); }
    public void disableComposition() {
        seeComposition.setEnabled(false); }
}
