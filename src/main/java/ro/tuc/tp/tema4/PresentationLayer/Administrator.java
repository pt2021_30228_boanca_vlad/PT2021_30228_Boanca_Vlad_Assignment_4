package ro.tuc.tp.tema4.PresentationLayer;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import ro.tuc.tp.tema4.BusinessLayer.MenuItem;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import java.util.HashMap;
/** Reprezinta interfata grafica utilizata de catre administrator pentru interactionarea cu programul. */
public class Administrator extends JFrame {
    private JTable productsTable;
    private JScrollPane scrollPane;
    private JLabel compositeProductItems;
    private JButton importProductsButton;
    private JButton addProductButton;
    private JButton editProductButton;
    private JButton deleteProductButton;
    private JButton createProductButton;
    private JButton generateReportsButton;
    private JButton addToComposedProduct;
    private JButton logOut;
    private JButton seeOrders;

    /** Creaza un obiect din clasa Administratot */
    public Administrator(HashMap<Integer, MenuItem> products) {
        compositeProductItems = new JLabel("0 products in composition");
        importProductsButton = new JButton("Import products");
        addProductButton = new JButton("Add product");
        editProductButton = new JButton("Edit selected product");
        deleteProductButton = new JButton("Detele selected product");
        createProductButton = new JButton("Create product");
        generateReportsButton = new JButton("Generate reports...");
        addToComposedProduct = new JButton("Add to new product");
        logOut = new JButton("Logout");
        seeOrders = new JButton("See orders");
        editProductButton.setEnabled(false);
        deleteProductButton.setEnabled(false);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setTitle("Admin Window");
        this.setSize(1050, 600);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        initializePanel(products);
        this.setVisible(true);
    }
    /** Initializeaza continutul ferestrei */
    public void initializePanel(HashMap<Integer, MenuItem> products) {
        createTable(products);
        FormLayout layout = new FormLayout(
                "20px, 800px, 20px, 190px, 20px",
                "20px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 150px, 30px, 10px, 30px, 10px, 30px, 20px");
        PanelBuilder builder = new PanelBuilder(layout);
        CellConstraints cc = new CellConstraints();
        builder.add(scrollPane, cc.xywh(2, 2, 1, 19));
        builder.add(importProductsButton, cc.xyw(4, 2, 1));
        builder.add(addProductButton, cc.xyw(4, 4, 1));
        builder.add(editProductButton, cc.xyw(4, 6, 1));
        builder.add(deleteProductButton, cc.xyw(4, 8, 1));
        builder.add(generateReportsButton, cc.xyw(4, 10, 1));
        builder.add(addToComposedProduct, cc.xyw(4, 12, 1));
        builder.add(compositeProductItems, cc.xyw(4, 16, 1));
        builder.add(createProductButton, cc.xyw(4, 18, 1));
        builder.add(logOut, cc.xyw(4, 20, 1));

        this.setContentPane(builder.getPanel());
        this.repaint();
        this.revalidate();
    }

    /** Creaza tabelul care contine produsele existente */
    public void createTable(HashMap<Integer, MenuItem> products) {
        String[] col = {"Title", "Rating", "Calories", "Proteins", "Fat", "Sodium", "Price", "Hashcode"};
        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        productsTable = new JTable(tableModel);
        if(products != null) {
            for(MenuItem p : products.values()) {
                Object[] arr = {p.getTitle(), p.getRating(), p.getCalories(), p.getProteins(), p.getFat(), p.getSodium(), p.getPrice(), p.hashCode()};
                tableModel.addRow(arr);
            }
            TableColumnModel tcm = productsTable.getColumnModel();
            tcm.removeColumn(tcm.getColumn(7));
        }
        scrollPane = new JScrollPane(productsTable);
    }

    public void enableFiltering() {
        editProductButton.setEnabled(true);
        deleteProductButton.setEnabled(true);
    }
    public JButton getImportProductsButton() {
        return importProductsButton; }
    public JButton getAddProductButton() {
        return addProductButton; }
    public JButton getEditProductButton() {
        return editProductButton; }
    public JButton getDeleteProductButton() {
        return deleteProductButton; }
    public JButton getGenerateReportsButton() {
        return generateReportsButton; }
    public JButton getLogOutButton() {
        return logOut; }
    public int getSelectedRowHashValue() {
        return (int) productsTable.getModel().getValueAt(productsTable.getSelectedRow(), 7); }
    public JButton getAddToComposedProduct() {
        return addToComposedProduct; }
    public JButton getCreateProductButton() {
        return createProductButton; }
    public void setCompositeProductItems(String text) {
        this.compositeProductItems.setText(text); }
}
