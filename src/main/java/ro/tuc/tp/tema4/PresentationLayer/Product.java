package ro.tuc.tp.tema4.PresentationLayer;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import ro.tuc.tp.tema4.BusinessLayer.MenuItem;

import javax.swing.*;
/** Reprezinta interfata grafica utilizata de catre administrator pentru adaugarea / modificarea / stergerea produselor. */
public class Product extends JFrame {

    private JTextField title;
    private JTextField rating;
    private JTextField calories;
    private JTextField proteins;
    private JTextField fat;
    private JTextField sodium;
    private JTextField price;
    private JButton submitButton;

    /** Creaza o fereastra pentru adaugarea unui nou produs. */
    public Product(String windowTitle) {
        title = new JTextField();
        rating = new JTextField();
        calories = new JTextField();
        proteins = new JTextField();
        fat = new JTextField();
        sodium = new JTextField();
        price = new JTextField();
        submitButton = new JButton("Submit");
        this.setTitle(windowTitle);
        this.setSize(320, 380);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        initializePanel();
        this.setVisible(true);
    }

    /** Creaza o fereastra pentru editarea unui produs deja existent. */
    public Product(String windowTitle, MenuItem product) {
        title = new JTextField(product.getTitle());
        rating = new JTextField(String.valueOf(product.getRating()));
        calories = new JTextField(String.valueOf(product.getCalories()));
        proteins = new JTextField(String.valueOf(product.getProteins()));
        fat = new JTextField(String.valueOf(product.getFat()));
        sodium = new JTextField(String.valueOf(product.getSodium()));
        price = new JTextField(String.valueOf(product.getPrice()));
        submitButton = new JButton("Submit");
        this.setTitle(windowTitle);
        this.setSize(320, 380);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        initializePanel();
        this.setVisible(true);
    }

    /** Initializeaza interfata grafica. */
    private void initializePanel() {
        FormLayout layout = new FormLayout(
                "20px, right:70px, 10px, 200px, 20px",
                "20px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 10px, 30px, 20px");
        PanelBuilder builder = new PanelBuilder(layout);
        CellConstraints cc = new CellConstraints();
        builder.add(new JLabel("Title"), cc.xyw(2, 2, 1));
        builder.add(title, cc.xyw(4, 2, 1));
        builder.add(new JLabel("Rating"), cc.xyw(2, 4, 1));
        builder.add(rating, cc.xyw(4, 4, 1));
        builder.add(new JLabel("Calories"), cc.xyw(2, 6, 1));
        builder.add(calories, cc.xyw(4, 6, 1));
        builder.add(new JLabel("Proteins"), cc.xyw(2, 8, 1));
        builder.add(proteins, cc.xyw(4, 8, 1));
        builder.add(new JLabel("Fat"), cc.xyw(2, 10, 1));
        builder.add(fat, cc.xyw(4, 10, 1));
        builder.add(new JLabel("Sodium"), cc.xyw(2, 12, 1));
        builder.add(sodium, cc.xyw(4, 12, 1));
        builder.add(new JLabel("Price"), cc.xyw(2, 14, 1));
        builder.add(price, cc.xyw(4, 14, 1));
        builder.add(submitButton, cc.xyw(2, 16, 3));
        this.setContentPane(builder.getPanel());
    }

    public JButton getSubmitButton() {
        return submitButton;
    }
    public Integer getCaloriesValue() {
        return Integer.parseInt(calories.getText());
    }
    public Integer getFatValue() {
        return Integer.parseInt(fat.getText());
    }
    public Integer getPriceValue() {
        return Integer.parseInt(price.getText());
    }
    public Integer getProteinsValue() {
        return Integer.parseInt(proteins.getText());
    }
    public Double getRatingValue() {
        return Double.parseDouble(rating.getText());
    }
    public Integer getSodiumValue() {
        return Integer.parseInt(sodium.getText());
    }
    public String getTitleValue() {
        return title.getText();
    }
}
