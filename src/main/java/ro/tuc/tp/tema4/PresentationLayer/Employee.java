package ro.tuc.tp.tema4.PresentationLayer;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import ro.tuc.tp.tema4.BusinessLayer.MenuItem;
import ro.tuc.tp.tema4.BusinessLayer.Order;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observer;
/** Reprezinta interfata grafica utilizata de catre utilizator pentru autentificarea in program. */
public class Employee extends JFrame implements Observer {

    private JScrollPane scrollPane;
    private JTable ordersTable;
    private JButton seeSelectedOrder;
    private JButton logOut;
    private HashMap<Order, ArrayList<MenuItem>> orders;

    /** Creaza un obiect din clasa Employee
     * @param orders Comenzile existente */
    public Employee(HashMap<Order, ArrayList<MenuItem>> orders) {
        seeSelectedOrder = new JButton("See selected order");
        logOut = new JButton("Logout");
        this.orders = orders;
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setTitle("Employee Window");
        this.setSize(850, 500);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        initializePanel(orders);
        this.setVisible(true);
    }

    /** Initializeaza continutul ferestrei utilizata de catre angajat.
     * @param orders Comenzile existente */
    private void initializePanel(HashMap<Order, ArrayList<MenuItem>> orders) {
        FormLayout layout = new FormLayout(
                "20px, 600px, 20px, 190px, 20px",
                "20px, 30px, 20px, 350px, 10px, 30px, 20px");
        PanelBuilder builder = new PanelBuilder(layout);
        CellConstraints cc = new CellConstraints();
        createTable(orders);
        builder.add(scrollPane, cc.xywh(2, 2, 1, 5));
        builder.add(seeSelectedOrder, cc.xyw(4, 2, 1));
        builder.add(logOut, cc.xyw(4, 6, 1));
        this.setContentPane(builder.getPanel());
        this.repaint();
        this.revalidate();
    }

    /** Creaza tabelul care contine comenzile existente.
     * @param orders Comenzile existente */
    public void createTable(HashMap<Order, ArrayList<MenuItem>> orders) {
        String[] col = {"Order ID", "Client ID", "Order Date", "Price", "Hashcode"};
        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        ordersTable = new JTable(tableModel);
        if(orders != null) {
            for(Order o : orders.keySet()) {
                Object[] arr = {o.getOrderID(), o.getClientID(), o.getOrderDate(), o.getPrice(), o.hashCode()};
                tableModel.addRow(arr);
            }
            TableColumnModel tcm = ordersTable.getColumnModel();
            tcm.removeColumn(tcm.getColumn(4));
        }
        scrollPane = new JScrollPane(ordersTable);
    }

    public JButton getSeeSelectedOrderButton() {
        return seeSelectedOrder; }
    public JButton getLogOutButton() {
        return logOut; }
    public Order getSelectedOrder() {
        return orders.keySet().stream().filter(o -> o.hashCode() == (Integer)ordersTable.getModel().getValueAt(ordersTable.getSelectedRow(), 4)).findFirst().orElse(null); }

    @Override
    public void update(java.util.Observable o, Object arg) {
        initializePanel(orders);
        JOptionPane.showMessageDialog(this, ((Order)arg).toString());
    }
}
