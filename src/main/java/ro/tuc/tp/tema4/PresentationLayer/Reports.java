package ro.tuc.tp.tema4.PresentationLayer;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import javax.swing.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/** Reprezinta interfata grafica utilizata de catre administrator pentru generarea rapoartelor. */
public class Reports extends JFrame {

    private JComboBox<Object> minHour;
    private JComboBox<Object> maxHour;
    private JTextField minProductAmountB;
    private JTextField orderAmount;
    private JTextField minPrice;
    private JTextField date;
    private JButton generateReportA;
    private JButton generateReportB;
    private JButton generateReportC;
    private JButton generateReportD;

    /** Creaza un obiect din clasa Reports. */
    public Reports() {
        minHour = new JComboBox<>();
        maxHour = new JComboBox<>();
        minProductAmountB = new JTextField();
        orderAmount = new JTextField();
        minPrice = new JTextField();
        date = new JTextField();
        minHour.addItem("Select beginning hour");
        maxHour.addItem("Select ending hour");
        generateReportA = new JButton("Generate report");
        generateReportB = new JButton("Generate report");
        generateReportC = new JButton("Generate report");
        generateReportD = new JButton("Generate report");
        for(int i = 0; i < 24; i++) {
            minHour.addItem(i);
            maxHour.addItem(i);
        }
        this.setTitle("Generate reports");
        this.setSize(1000, 240);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        initializePanel();
        this.setVisible(true);
    }

    /** Initializeaza interfata grafica. */
    private void initializePanel() {
        FormLayout layout = new FormLayout(
                "20px, 180px, 10px, 180px, 10px, 180px, 10px, 180px, 10px, 180px, 10px, 150px, 20px",
                "20px, 30px, 20px, 30px, 20px, 30px, 20px, 30px, 20px");
        PanelBuilder builder = new PanelBuilder(layout);
        CellConstraints cc = new CellConstraints();
        builder.add(new JLabel("Time interval of the orders:"), cc.xyw(2, 2, 1));
        builder.add(minHour, cc.xyw(4, 2, 1));
        builder.add(maxHour, cc.xyw(6, 2, 1));
        builder.add(generateReportA, cc.xyw(10, 2, 1));

        builder.add(new JLabel("Products ordered more than"), cc.xyw(2, 4, 1));
        builder.add(minProductAmountB, cc.xyw(4, 4, 1));
        builder.add(new JLabel("times."), cc.xyw(6, 4, 1));
        builder.add(generateReportB, cc.xyw(10, 4, 1));

        builder.add(new JLabel("Clients with at least"), cc.xyw(2, 6, 1));
        builder.add(orderAmount, cc.xyw(4, 6, 1));
        builder.add(new JLabel("orders with the price at least"), cc.xyw(6, 6, 1));
        builder.add(minPrice, cc.xyw(8, 6, 1));
        builder.add(generateReportC, cc.xyw(10, 6, 1));

        builder.add(new JLabel("Products ordered in "), cc.xyw(2, 8, 1));
        builder.add(date, cc.xyw(4, 8, 1));
        builder.add(new JLabel(", minimum orders: "), cc.xyw(6, 8, 1));
        builder.add(generateReportD, cc.xyw(10, 8, 1));

        this.setContentPane(builder.getPanel());
    }

    public JButton getGenerateReportAButton() {
        return generateReportA; }
    public JButton getGenerateReportBButton() {
        return generateReportB; }
    public JButton getGenerateReportCButton() {
        return generateReportC; }
    public JButton getGenerateReportDButton() {
        return generateReportD; }
    public int getMinHour() {
        return minHour.getSelectedIndex() - 1; }
    public int getMaxHour() {
        return maxHour.getSelectedIndex() - 1; }
    public Date getDate() throws ParseException {
        return new SimpleDateFormat("dd/MM/yyyy").parse(date.getText()); }
    public int getOrderAmount() {
        return Integer.parseInt(orderAmount.getText()); }
    public int getMinPrice() {
        return Integer.parseInt(minPrice.getText()); }
    public int getMinProductAmountB() {
        return Integer.parseInt(minProductAmountB.getText()); }
}
